#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cstdlib>

#include <iostream>
#include <fstream>
#include <string>
#include <cassert>

#include <sstream>

#include <unistd.h>
#include <stdio.h>
#include <limits.h>

#include "buffer.c"

#include "Renderer.h"

#include "VertexBuffer.h"
#include "VertexBufferLayout.h"
#include "IndexBuffer.h"
#include "VertexArray.h"
#include "Shader.h"
#include "Texture.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

int pwd() {
    char cwd[PATH_MAX];
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
        printf("Current working dir: %s\n", cwd);
    } else {
        perror("getcwd() error");
        return 1;
    }
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}

int main(void) {
    GLFWwindow *window;

    /* Initialize the library */
    if (!glfwInit())
        return -1;

    // glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    // glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    // glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(960, 540, "Hello World", NULL, NULL);
    if (!window) {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    /* Synchronise draws with the monitors refresh rate or with the vsync */
    glfwSwapInterval(1);

    if (glewInit() != GLEW_OK)
        std::cout << "Error initializing glew!" << std::endl;

    std::cout << "OpenGL Version: " << glGetString(GL_VERSION) << std::endl;

    // Define a buffer
    float vertices[] = {
            100.0f, 100.0f, 0.0f, 0.0f,
            400.0f, 100.0f, 1.0f, 0.0f,
            400.0f, 400.0f, 1.0f, 1.0f,
            100.0f, 400.0f, 0.0f, 1.0f,
    };

    unsigned int indices[6] = {0, 1, 2, 2, 3, 0};

    // Enable opacity blending. Multiply src color with our alpha and
    // the destination color in the buffer with 1 - alpha
    GLCall(glEnable(GL_BLEND));
    GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

    VertexArray va;
    int amountOfVertices = 4;
    int elementsInVertice = 4;
    VertexBuffer vb(vertices, amountOfVertices * elementsInVertice * sizeof(float));

    VertexBufferLayout layout;
    layout.Push<float>(2);
    layout.Push<float>(2);
    va.AddBuffer(vb, layout);

    IndexBuffer ib(indices, 6);

    // Create a projection matrix
    // -> left edge, right edge, bottom edge, top edge, near and far plane
    // stuff outside the planes are culled :O
    glm::mat4 proj = glm::ortho(0.0f, 960.0f, 0.0f, 540.0f, -1.0f, 1.0f);

    pwd();
    Shader shader("../src/shaders/Basic.glsl");
    shader.Bind();

    // After the shader is bound
    shader.SetUniform4f("u_Color", 0.2f, 0.3f, 0.8f, 1.0f);
    shader.SetUniform4f("u_Location", 0.0f, 0.0f, 0.0f, 0.0f);
    shader.SetUniform2f("u_texSearch", 0.0f, 0.0f);
    shader.SetUniformMat4f("u_MVP", proj);

    Texture texture("../res/textures/opengl.png");
    int textureSlot = 0;
    texture.Bind(textureSlot);
    shader.SetUniform1i("u_Texture", textureSlot);

    // Unbind anything bound before
    va.Unbind();
    vb.Unbind();
    ib.Unbind();
    shader.Unbind();

    Renderer renderer;

    float r = 0.0f;
    float increment = 0.05f;

    float x = 0.0f;
    float y = 0.0f;
    float dx = 0.00523f;
    float dy = 0.011234f;

    float sx = -2.0f;
    float sy = 0.0f;
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        processInput(window);

        /* Render here */
        renderer.Clear();

        /* Bind our shader
           Setup our uniforms
           Bind our vertex buffer
           Setup the layout of that vertex buffer (via attributes)
           Bind our index buffer
           Then draw glElements
           --> changes to
           Bind our shader
           Uniforms
           Bind our vertex array
           Bind our index buffer
           Issue draw call
        */
        shader.Bind();
        shader.SetUniform4f("u_Color", r, 0.3f, 0.8f, 1.0f);
        shader.SetUniform4f("u_Location", x, y, 0.0f, 0.0f);
        //shader.SetUniform2f("u_texSearch", sx, sy);
        if(sx < 2){
            sx += 0.02f;
        }
        std::cout << sx << " : " << sy << std::endl;

        renderer.Draw(va, ib, shader);

        if (r > 1.0f) {
            increment = -0.05f;
        } else if (r < 0.0f) {
            increment = 0.05f;
        }

        if (x >= 0.5f || x <= -0.5f) {
            dx *= -1;
        }
        if (y >= 0.5f || y <= -0.5f) {
            dy *= -1;
        }

        r += increment;
        x += dx;
        y += dy;

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    glfwTerminate();
    std::cout << "Exiting, bye bye!" << std::endl;
    return 0;
}
