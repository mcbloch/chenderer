#shader vertex
#version 320 es

precision mediump float;

layout(location = 0) in vec2 a_position;
layout(location = 1) in vec2 a_texCoord;

out vec2 v_TexCoord;

uniform vec4 u_Location;
uniform vec2 u_texSearch;

// Model view projection matrix
uniform mat4 u_MVP;

void main()
{
//     myPosition -= vec2(100.0f, 400.0f);
     gl_Position = (u_MVP * vec4(a_position, 0.0f, 1.0f));
     gl_Position += u_Location;
//     gl_Position *= 2.0f;
//     gl_Position -= 1.0f;
     v_TexCoord = a_texCoord;
//     v_TexCoord.y = a_texCoord.x;
//     v_TexCoord.x = a_texCoord.y;
//     v_TexCoord.y += 0.5f;
     //v_TexCoord.y -= 150.0f;
     //v_TexCoord.y += u_texSearch.x;
}

#shader fragment
#version 320 es

precision mediump float;

layout(location = 0) out vec4 color;

in vec2 v_TexCoord;

uniform vec4 u_Color;
uniform sampler2D u_Texture;

void main()
{
     mediump vec4 texColor = texture2D(u_Texture, v_TexCoord);
//     color = vec4(texColor.xyz, 1.0f);
//
//     if (texColor.x == 0.0f){
//          texColor = vec4(1.0, 0.0, 0.0, 1.0);
//     } else {
//          texColor = vec4(0.0, 1.0, 0.0, 1.0);
//     }

     color = texColor; // + u_Color;
     if(color.a != 0.0f){
          color.x = color.x * 0.8 + u_Color.x * 0.2;
          color.y = color.x * 0.8 + u_Color.y * 0.2;
          color.z = color.x * 0.8 + u_Color.z * 0.2;
     } else {
//         color = vec4(1.0f);
     }
//     color = vec4(vec3(v_TexCoord.y), 1.0f);
}