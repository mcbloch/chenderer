#shader vertex
#version 320 es

layout(location = 0) in vec4 position;

uniform highp vec4 u_Location;

void main(){
     gl_Position = position + u_Location;
}

#shader fragment
#version 320 es

layout(location = 0) out highp vec4 color;

uniform highp vec4 u_Color;

void main(){
     color = u_Color;
}