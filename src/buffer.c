
typedef struct Vertice {
  float position[2];
  float texCoord[2];
} Vertice;
