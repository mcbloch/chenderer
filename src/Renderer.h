#pragma once
//
// Created by maxime on 9/29/19.
//

#include <GL/glew.h>

#include <csignal>
#include <string>

#define ASSERT(x)                                                              \
  if (!(x))                                                                    \
    std::raise(SIGINT);

#ifdef DEBUG
#define GLCall(x)                                                              \
  GLClearError();                                                              \
  x;                                                                           \
  ASSERT(GLLogCall(#x, __FILE__, __LINE__))
#else
#define GLCall(x) x
#endif

#include "VertexArray.h"
#include "IndexBuffer.h"
#include "Shader.h"

void GLClearError();
std::string GetGLErrorStr(GLenum err);
bool GLLogCall(const char *function, const char *file, int line);

class Renderer {
public:
    void Clear() const;
    void Draw(const VertexArray& va, const IndexBuffer& ib, const Shader& shader) const;
};