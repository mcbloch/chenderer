Debugging can be done via the debugging target of the makefile. Compilation is by default with gdb debug flags
- the print the backtrice with 'bt'

You can break in the code by using the ASSERT function.

So a crash can be examined with

	make
	make debug
	> run
	*applcation gives a breakpoint interrupt*
	> bt
